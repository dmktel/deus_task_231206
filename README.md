# deus_task_231206

**Light:**
1. Deploy 2 linux VMs: web01 and files01. You can use any creation methods, for example, in Yandex Cloud or locally on Vagrant.
2. Install the NFS server on the files01 virtual machine. Configure the export of the /files directory.
3. Install the NFS client on the "web01" virtual machine. Set up a connection to the server and get the directories from the server.
4. Install and configure nginx on the "web01" virtual machine. Set up the server as a proxy on localhost.
5. Install Django on "web 01", run https://gitfront.io/r/deusops/BC6tmrogTrbh/django-filesharing.git and check that everything is working.
6. Configure the server so that files are transferred to the NFS storage via the website.

**Prerequisites:**

1. Vagrant v2.4 installed
2. Vagrant provider: virtualbox
3. Vagrant box 'bento/ubuntu-22.04'

---

**Normal:**

1. Create a Dockerfile for the application, install Docker on "web1" and run the application as configured in the Light tier.
2. Create a gitlab-ci payplane to automatically build and publish a Docker image with the application
3. Create ansible-playbook customizing web01 and files01 server
4. Create ansible-role "nfs", to install nfs-server and nfs-client. Figure out how to separate Server and Client at the role level and at the inventory-host-file level.
5. Create ansible-role "nginx" to install nginx and a template to configure proxy on localhost
6. Create ansible role "docker" to install docker and a template to run docker-compose with the application we need.
7. Add a step to gitlab-ci with the application deploy to the servers. Ansible roles should be in separate repositories and run through galaxy.
